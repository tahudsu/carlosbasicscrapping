<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\readContentUrlService;
use App\Entity\BodyData;

class DefaultController extends Controller
{
    public function index(readContentUrlService $readContent)
    {
        $allBodyData = $this->getDoctrine()->getRepository(BodyData::class)->findAll();
        $counter = 0;
        array_map(function($bodyData) use ($readContent, &$counter) {
            // sino tenemos url no hacemos nada con el objeto
            if ($bodyData->getUrl()) {
                $readContent->getContentUrl($bodyData);
                $counter += 1;
            }
        }, $allBodyData);

        return $this->render('index.html.twig', ['counter' => $counter]);
    }
}