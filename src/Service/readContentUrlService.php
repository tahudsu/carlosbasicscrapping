<?php

namespace App\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use App\Entity\BodyData;
use Doctrine\ORM\EntityManagerInterface;

class readContentUrlService
{
    protected $url;
    protected $bodyData;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getContentUrl(BodyData $bodyData)
    {
        $this->bodyData = $bodyData;
        try {
            $request = new Request('GET', $this->bodyData->getUrl());
            $client = new Client();
            $response = $client->send($request);
            $body = $response->getBody()->getContents();
        } catch(\Exception $e) {
            throw new \Exception("Error Processing Request", $e->getMessage() . ' the trace <br>' . $e->getTraceAsString());
        }

        $this->responseToObject($body);
    }

    protected function responseToObject($body)
    {
        $this->bodyData->parseXmlData($body);
        $this->em->getRepository(BodyData::class);
        $this->em->persist($this->bodyData);
        $this->em->flush();
    }
}