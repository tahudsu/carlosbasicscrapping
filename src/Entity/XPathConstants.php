<?php
namespace App\Entity;

class XPathConstants {
    public $constants = [
        'idTipoOrg' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idTipoOrg"]',
        'idOrg' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idOrg"]',
        'idNumDoc' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idNumDoc"]',
        'menuTipoIdioma' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:menuTipoIdioma"]',
        'textSSite__' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:textSSite__"]',
        'idActividades' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idActividades"]',
        'idVia' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idVia"]',
        'idCP' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idCP"]',
        'idPoblac' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idPoblac"]',
        'idPais' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idPais"]',
        'idTlf' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idTlf"]',
        'idFax' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idFax"]',
        'idMail' => '//*[@id="viewns_Z7_AVEQAI930GRPE02BR764FO30G0_:perfilComp:idMail"]'
    ];
}
