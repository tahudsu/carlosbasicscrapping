<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity("url")
 * @ORM\Entity(repositoryClass="App\Repository\BodyDataRepository")
 */
class BodyData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $url;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idTipoOrg;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idOrg;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idNumDoc;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $menuTipoIdioma;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $textSSite__;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idActividades;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idVia;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idCP;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idPoblac;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idPais;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idTlf;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idFax;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $idMail;

    public function constructor() {}

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of url
     *
     * @return  string
     */ 
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of url
     *
     * @param  string  $url
     *
     * @return  self
     */ 
    public function setUrl(?string $url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of idTipoOrg
     *
     * @return  string
     */ 
    public function getIdTipoOrg()
    {
        return $this->idTipoOrg;
    }

    /**
     * Set the value of idTipoOrg
     *
     * @param  string  $idTipoOrg
     *
     * @return  self
     */ 
    public function setIdTipoOrg(?string $idTipoOrg)
    {
        $this->idTipoOrg = $idTipoOrg;

        return $this;
    }

    /**
     * Get the value of idOrg
     *
     * @return  string
     */ 
    public function getIdOrg()
    {
        return $this->idOrg;
    }

    /**
     * Set the value of idOrg
     *
     * @param  string  $idOrg
     *
     * @return  self
     */ 
    public function setIdOrg(?string $idOrg)
    {
        $this->idOrg = $idOrg;

        return $this;
    }

    /**
     * Get the value of idNumDoc
     *
     * @return  string
     */ 
    public function getIdNumDoc()
    {
        return $this->idNumDoc;
    }

    /**
     * Set the value of idNumDoc
     *
     * @param  string  $idNumDoc
     *
     * @return  self
     */ 
    public function setIdNumDoc(?string $idNumDoc)
    {
        $this->idNumDoc = $idNumDoc;

        return $this;
    }

    /**
     * Get the value of menuTipoIdioma
     *
     * @return  string
     */ 
    public function getMenuTipoIdioma()
    {
        return $this->menuTipoIdioma;
    }

    /**
     * Set the value of menuTipoIdioma
     *
     * @param  string  $menuTipoIdioma
     *
     * @return  self
     */ 
    public function setMenuTipoIdioma(?string $menuTipoIdioma)
    {
        $this->menuTipoIdioma = $menuTipoIdioma;

        return $this;
    }

    /**
     * Get the value of textSSite__
     *
     * @return  string
     */ 
    public function getTextSSite__()
    {
        return $this->textSSite__;
    }

    /**
     * Set the value of textSSite__
     *
     * @param  string  $textSSite__
     *
     * @return  self
     */ 
    public function setTextSSite__(?string $textSSite__)
    {
        $this->textSSite__ = $textSSite__;

        return $this;
    }

    /**
     * Get the value of idActividades
     *
     * @return  string
     */ 
    public function getIdActividades()
    {
        return $this->idActividades;
    }

    /**
     * Set the value of idActividades
     *
     * @param  string  $idActividades
     *
     * @return  self
     */ 
    public function setIdActividades(?string $idActividades)
    {
        $this->idActividades = $idActividades;

        return $this;
    }

    /**
     * Get the value of idVia
     *
     * @return  string
     */ 
    public function getIdVia()
    {
        return $this->idVia;
    }

    /**
     * Set the value of idVia
     *
     * @param  string  $idVia
     *
     * @return  self
     */ 
    public function setIdVia(?string $idVia)
    {
        $this->idVia = $idVia;

        return $this;
    }

    /**
     * Get the value of idCP
     *
     * @return  string
     */ 
    public function getIdCP()
    {
        return $this->idCP;
    }

    /**
     * Set the value of idCP
     *
     * @param  string  $idCP
     *
     * @return  self
     */ 
    public function setIdCP(?string $idCP)
    {
        $this->idCP = $idCP;

        return $this;
    }

    /**
     * Get the value of idPoblac
     *
     * @return  string
     */ 
    public function getIdPoblac()
    {
        return $this->idPoblac;
    }

    /**
     * Set the value of idPoblac
     *
     * @param  string  $idPoblac
     *
     * @return  self
     */ 
    public function setIdPoblac(?string $idPoblac)
    {
        $this->idPoblac = $idPoblac;

        return $this;
    }

    /**
     * Get the value of idPais
     *
     * @return  string
     */ 
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * Set the value of idPais
     *
     * @param  string  $idPais
     *
     * @return  self
     */ 
    public function setIdPais(?string $idPais)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get the value of idTlf
     *
     * @return  string
     */ 
    public function getIdTlf()
    {
        return $this->idTlf;
    }

    /**
     * Set the value of idTlf
     *
     * @param  string  $idTlf
     *
     * @return  self
     */ 
    public function setIdTlf(?string $idTlf)
    {
        $this->idTlf = $idTlf;

        return $this;
    }

    /**
     * Get the value of idFax
     *
     * @return  string
     */ 
    public function getIdFax()
    {
        return $this->idFax;
    }

    /**
     * Set the value of idFax
     *
     * @param  string  $idFax
     *
     * @return  self
     */ 
    public function setIdFax(?string $idFax)
    {
        $this->idFax = $idFax;

        return $this;
    }

    /**
     * Get the value of idMail
     *
     * @return  string
     */ 
    public function getIdMail()
    {
        return $this->idMail;
    }

    /**
     * Set the value of idMail
     *
     * @param  string  $idMail
     *
     * @return  self
     */ 
    public function setIdMail(?string $idMail)
    {
        $this->idMail = $idMail;

        return $this;
    }

    public function parseXmlData($body)
    {
        $doc = new \DOMDocument(null, 'UTF-8');
        $internalErrors = libxml_use_internal_errors(true);
        $doc->preserveWhiteSpace = false;

        $doc->loadHTML($body);

        $xpath = new \DOMXPath($doc);

        $tbody = $doc->getElementsByTagName('body')->item(0);
        $constants = new XPathConstants;
        $constants = $constants->constants;
        foreach ($constants as $key => $value) {
            $response = $xpath->query($value, $tbody);
           
            $nodeValue = ($response && $response->length) ? $response->item(0)->nodeValue : null;
            //guardamos en la propiedad el valor recogido del xpath
            $this->{'set' . ucfirst($key) }($nodeValue);
        }

        libxml_use_internal_errors($internalErrors);
    }
}
