instalados git y composer, mysql o mariadb.

1. git clone repo

2. composer install

en el archivo .env.dev
linea 28 DATABASE_URL, se puede cambiar la configuracion de bbdd, compuesta por usuario:password@ipservidormysql:puerto/basededatos
que se ejecutara a continuación, si ya existiera, no es necesario ejecutar el paso 3.

Si expusiera un error, es recomendable eliminar el contenido del directorio var/cache del proyecto

3. php bin/console doctrine:database:create

4. php bin/console doctrine:schema:update --force

Ya tendríamos la bbdd, podemos insertar urls de busqueda, dentro del campo url de la tabla body_data.

5. php bin/console server:run
corre el servidor web de desarollo que esta configurado para symfony, el propio servidor nos dice donde esta escuchando

6. navegar a 127.0.0.1:8000

Al finalizar, se habrá lanzado el script, si teniamos urls, habrá realizado cambios en la tabla, y nos dira cuantas urls ha pedido.

7. Para conseguir más, añadir más urls, y volver a lanzar el scrip a través del navegador. 

Atención, vuelve a realizar todas las peticiones, y machaca los datos que estén en la tabla, por los que recupere.